-- Revert aprt:auth_api from pg
\set api api

BEGIN;

drop schema if exists :api cascade;

COMMIT;
