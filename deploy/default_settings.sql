-- Deploy aprt:default_settings to pg
\set jwt_secret '7aKjb88pJsafjsw0CVN7t8S6ZYsA5ZWG'

BEGIN;

select settings.set('jwt_secret', :'jwt_secret');
select settings.set('jwt_lifetime', '3600');
select settings.set('auth.default-role', 'webuser');

COMMIT;
