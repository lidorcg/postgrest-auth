-- Deploy aprt:settings_schema to pg
BEGIN;

-- create settings schema
DROP SCHEMA IF EXISTS settings CASCADE;
CREATE SCHEMA settings;

-- create secrets table
CREATE TABLE settings.secrets (
    KEY text PRIMARY KEY,
    value text NOT NULL
);

-- create set and get functions to interact with secrets table
CREATE OR REPLACE FUNCTION settings.get (text)
    RETURNS text
AS $$
    SELECT value FROM settings.secrets WHERE KEY = $1
$$
SECURITY DEFINER STABLE
LANGUAGE sql;

CREATE OR REPLACE FUNCTION settings.set (text, text)
    RETURNS void
AS $$
    INSERT INTO settings.secrets (KEY, value) VALUES ($1, $2)
    ON CONFLICT (KEY) DO UPDATE SET value = $2;
$$
SECURITY DEFINER
LANGUAGE sql;

COMMIT;
