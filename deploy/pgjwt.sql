-- Deploy aprt:pgjwt to pg

BEGIN;

-- create the jwt schema namespace and all the functions in it
create extension if not exists pgcrypto;
drop schema if exists pgjwt cascade;
create schema pgjwt;
set search_path to pgjwt, public;

CREATE OR REPLACE FUNCTION url_encode(DATA bytea) RETURNS text LANGUAGE SQL AS $$
SELECT translate(encode(DATA, 'base64'), E'+/=\n', '-_'); $$;


CREATE OR REPLACE FUNCTION url_decode(DATA text) RETURNS bytea LANGUAGE SQL AS $$ WITH t AS
  (SELECT translate(DATA, '-_', '+/')),
                                                                                       rem AS
  (SELECT length(
                   (SELECT *
                    FROM t)) % 4) -- compute padding size

SELECT decode(
                (SELECT *
                 FROM t) || CASE WHEN
                (SELECT *
                 FROM rem) > 0 THEN repeat('=', (4 -
                                                   (SELECT *
                                                    FROM rem))) ELSE '' END, 'base64'); $$;


CREATE OR REPLACE FUNCTION algorithm_sign(signables text, secret text, algorithm text) RETURNS text LANGUAGE SQL AS $$ WITH alg AS
  ( SELECT CASE
               WHEN algorithm = 'HS256' THEN 'sha256'
               WHEN algorithm = 'HS384' THEN 'sha384'
               WHEN algorithm = 'HS512' THEN 'sha512'
               ELSE ''
           END) -- hmac throws error

SELECT pgjwt.url_encode(hmac(signables, secret,
                                     (SELECT *
                                      FROM alg))); $$;


CREATE OR REPLACE FUNCTION sign(payload json, secret text, algorithm text DEFAULT 'HS256') RETURNS text LANGUAGE SQL AS $$ WITH header AS
  ( SELECT pgjwt.url_encode(convert_to('{"alg":"' || algorithm || '","typ":"JWT"}', 'utf8')) ),
                                                                                                                                payload AS
  ( SELECT pgjwt.url_encode(convert_to(payload::text, 'utf8')) ),
                                                                                                                                signables AS
  ( SELECT
     (SELECT *
      FROM header) || '.' ||
     (SELECT *
      FROM payload) )
SELECT
  (SELECT *
   FROM signables) || '.' || pgjwt.algorithm_sign(
                                                          (SELECT *
                                                           FROM signables), secret, algorithm); $$;


CREATE OR REPLACE FUNCTION verify(token text, secret text, algorithm text DEFAULT 'HS256') RETURNS table(header json, payload json, VALID boolean) LANGUAGE SQL AS $$
SELECT convert_from(pgjwt.url_decode(r[1]), 'utf8')::json AS header,
       convert_from(pgjwt.url_decode(r[2]), 'utf8')::json AS payload,
       r[3] = pgjwt.algorithm_sign(r[1] || '.' || r[2], secret, algorithm) AS VALID
FROM regexp_split_to_array(token, '\.') r; $$;

set search_path to public;

COMMIT;
